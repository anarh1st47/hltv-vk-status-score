#!/usr/bin/python3
# By anarch1st47, 2015
# TODOne: auto-parse matchids from this link http://www.hltv.org/hltv.rss.php?pri=15
from socketIO_client import SocketIO, LoggingNamespace

import vk, sys, feedparser


if(len(sys.argv) == 2):
  listid = str(sys.argv[1]) # Номер матча, за которым следим, полученный из аргумента командной строки
else: # parse rss
  matches = [0, 0, 0]
  rss     = feedparser.parse('http://www.hltv.org/hltv.rss.php?pri=15')

  for i in range(3):
    matches[i] = rss['entries'][i]['link'][26:26+7]
    
  print(*matches)

group  = 61579968
last   = 100 #Номер раунда, который был отправлен в ВК
inp    = open("access_token")
token  = inp.read()

name   = " (HLTV.org VK scorebot(py)) "
#Открываем вконтачесессию
session= vk.Session(access_token=token[:85])
api    = vk.API(session)


def status(score):
  api.status.set(text=score)
  api.status.set(text=score, group_id=group)

def wall(score):
  print('null')
  api.wall.post(message=score, owner_id=-group)


def pr(*args):
  print('pr function started')
  global last #Используем глобальную переменную для записи
  scoreCT = args[0]['counterTerroristScore']
  scoreT  = args[0]['terroristScore']
  mapa    = args[0]['mapName']
  ttt     = args[0]['terroristTeamName']
  ccc     = args[0]['ctTeamName']
  score   = name + " (T) " + ttt + " " + str(scoreT) + " : " + str(scoreCT) + " " + ccc + " (CT) on map " + mapa #Формируем вывод в статус и лог
 
  
  if(last != scoreT+scoreCT): #Если этот раунд еще не отправлен в статус
    status(score)
    #api.status.set(text=score, access_token=token)
    last = scoreT + scoreCT
    if((scoreT >15 or scoreCT > 15) and last < 30):
      wall(score)
      score += " Posted to wall."
    if(last == 30):
      wall(score)
      score+=" Posted to wall."
    score+=". Posted to VK."
  print(score)



#Подключаемся к серверу hltv
print("len of args: ", len(sys.argv))
if(len(sys.argv) == 2):
  with SocketIO('http://scorebot2.hltv.org', 10022, LoggingNamespace) as socketIO:
    socketIO.on('connect', (lambda *args: exec("socketIO.on('scoreboard', pr);socketIO.emit('readyForMatch', listid)")))
    socketIO.wait(seconds=1*60*60)
else:
  for i in range(3600):          #1 hour
    with SocketIO('http://scorebot2.hltv.org', 10022, LoggingNamespace) as socketIO:
      socketIO.on('connect', (lambda *args: exec("print('matchid: ', matches[i%3]);\
                                                  socketIO.on('scoreboard', pr);\
                                                  socketIO.emit('readyForMatch', matches[i%3]);\
                                                 ")))
      socketIO.wait(seconds=3)
      socketIO.disconnect()
      print(matches[i%3])
      
print("Job succefully done.")
