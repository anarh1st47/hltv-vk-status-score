# README #

HLTV VK scorebot 

### What is this repository for? ###

* Easy parse hltv matches score to VK status

### How to? ###


* **You can use python version OR javascript version.** 

* **JS version:**

* * Installation


* * *  open react-scorebot.js in same text editor(gedit, for example) and change example token to your vk access_token(if you haven't access_token you can take it in same vk app, for example, in our app (https://oauth.vk.com/authorize?client_id=4347777&scope=status,wall,offline&redirect_uri=blank.html&display=popup&response_type=token))


* * Start using:


* * * open hltv.html in your browser


* * * write list id in textbox(for example, for match with url http://www.hltv.org/match/2299465-k1ck-onlinebots-alientech-allstars listid == 2299465)


* * * click OK button


* **Python version:**

* * Installation:


* * * Install modules:


* * * * VK (pip install -U vk)


* * * *  socketIO-client (pip install -U socketIO-client)


* * * Write VK access token to file "access_token"


* * Start using:


* * * start python script socket.io.py with listid parametr, for example python3 socket.io.py 2299465